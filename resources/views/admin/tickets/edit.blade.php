@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.ticket.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.tickets.update", [$ticket->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="user_id">{{ trans('cruds.ticket.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id" required>
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ (old('user_id') ? old('user_id') : $ticket->user->id ?? '') == $id ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
                @if($errors->has('user'))
                    <span class="text-danger">{{ $errors->first('user') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.ticket.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="lottery_id">{{ trans('cruds.ticket.fields.lottery') }}</label>
                <select class="form-control select2 {{ $errors->has('lottery') ? 'is-invalid' : '' }}" name="lottery_id" id="lottery_id" required>
                    @foreach($lotteries as $id => $lottery)
                        <option value="{{ $id }}" {{ (old('lottery_id') ? old('lottery_id') : $ticket->lottery->id ?? '') == $id ? 'selected' : '' }}>{{ $lottery }}</option>
                    @endforeach
                </select>
                @if($errors->has('lottery'))
                    <span class="text-danger">{{ $errors->first('lottery') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.ticket.fields.lottery_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="price">{{ trans('cruds.ticket.fields.price') }}</label>
                <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="number" name="price" id="price" value="{{ old('price', $ticket->price) }}" step="1" required>
                @if($errors->has('price'))
                    <span class="text-danger">{{ $errors->first('price') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.ticket.fields.price_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection