<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToFavouritesTable extends Migration
{
    public function up()
    {
        Schema::table('favourites', function (Blueprint $table) {
            $table->unsignedBigInteger('lottery_id');
            $table->foreign('lottery_id', 'lottery_fk_3347582')->references('id')->on('lotteries');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id', 'user_fk_3347583')->references('id')->on('users');
        });
    }
}
