@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.favourite.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.favourites.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="lottery_id">{{ trans('cruds.favourite.fields.lottery') }}</label>
                <select class="form-control select2 {{ $errors->has('lottery') ? 'is-invalid' : '' }}" name="lottery_id" id="lottery_id" required>
                    @foreach($lotteries as $id => $lottery)
                        <option value="{{ $id }}" {{ old('lottery_id') == $id ? 'selected' : '' }}>{{ $lottery }}</option>
                    @endforeach
                </select>
                @if($errors->has('lottery'))
                    <span class="text-danger">{{ $errors->first('lottery') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.favourite.fields.lottery_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="user_id">{{ trans('cruds.favourite.fields.user') }}</label>
                <select class="form-control select2 {{ $errors->has('user') ? 'is-invalid' : '' }}" name="user_id" id="user_id" required>
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ old('user_id') == $id ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
                @if($errors->has('user'))
                    <span class="text-danger">{{ $errors->first('user') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.favourite.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection