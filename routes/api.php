<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:sanctum']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Lotteries
    Route::post('lotteries/media', 'LotteryApiController@storeMedia')->name('lotteries.storeMedia');
    Route::apiResource('lotteries', 'LotteryApiController');

    // Currencies
    Route::apiResource('currencies', 'CurrencyApiController');

    // Favourites
    Route::apiResource('favourites', 'FavouriteApiController');

    // Tickets
    Route::apiResource('tickets', 'TicketsApiController');

    // Categories
    Route::apiResource('categories', 'CategoryApiController');
});
