@extends('layouts.admin')
@section('content')
@can('lottery_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.lotteries.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.lottery.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.lottery.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Lottery">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.lottery.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.lottery.fields.on_off') }}
                        </th>
                        <th>
                            {{ trans('cruds.lottery.fields.category') }}
                        </th>
                        <th>
                            {{ trans('cruds.lottery.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.lottery.fields.name_en') }}
                        </th>
                        <th>
                            {{ trans('cruds.lottery.fields.info') }}
                        </th>
                        <th>
                            {{ trans('cruds.lottery.fields.info_en') }}
                        </th>
                        <th>
                            {{ trans('cruds.lottery.fields.price') }}
                        </th>
                        <th>
                            {{ trans('cruds.lottery.fields.deadline') }}
                        </th>
                        <th>
                            {{ trans('cruds.lottery.fields.photo') }}
                        </th>
                        <th>
                            {{ trans('cruds.lottery.fields.winner') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <select class="search" strict="true">
                                <option value>{{ trans('global.all') }}</option>
                                @foreach(App\Models\Lottery::ON_OFF_SELECT as $key => $item)
                                    <option value="{{ $item }}">{{ $item }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <select class="search">
                                <option value>{{ trans('global.all') }}</option>
                                @foreach($categories as $key => $item)
                                    <option value="{{ $item->name }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                        </td>
                        <td>
                            <select class="search">
                                <option value>{{ trans('global.all') }}</option>
                                @foreach($users as $key => $item)
                                    <option value="{{ $item->email }}">{{ $item->email }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($lotteries as $key => $lottery)
                        <tr data-entry-id="{{ $lottery->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $lottery->id ?? '' }}
                            </td>
                            <td>
                                {{ App\Models\Lottery::ON_OFF_SELECT[$lottery->on_off] ?? '' }}
                            </td>
                            <td>
                                {{ $lottery->category->name ?? '' }}
                            </td>
                            <td>
                                {{ $lottery->name ?? '' }}
                            </td>
                            <td>
                                {{ $lottery->name_en ?? '' }}
                            </td>
                            <td>
                                {{ $lottery->info ?? '' }}
                            </td>
                            <td>
                                {{ $lottery->info_en ?? '' }}
                            </td>
                            <td>
                                {{ $lottery->price ?? '' }}
                            </td>
                            <td>
                                {{ $lottery->deadline ?? '' }}
                            </td>
                            <td>
                                @if($lottery->photo)
                                    <a href="{{ $lottery->photo->getUrl() }}" target="_blank" style="display: inline-block">
                                        <img src="{{ $lottery->photo->getUrl('thumb') }}">
                                    </a>
                                @endif
                            </td>
                            <td>
                                {{ $lottery->winner->email ?? '' }}
                            </td>
                            <td>
                                @can('lottery_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.lotteries.show', $lottery->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('lottery_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.lotteries.edit', $lottery->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('lottery_delete')
                                    <form action="{{ route('admin.lotteries.destroy', $lottery->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('lottery_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.lotteries.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Lottery:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
let visibleColumnsIndexes = null;
$('.datatable thead').on('input', '.search', function () {
      let strict = $(this).attr('strict') || false
      let value = strict && this.value ? "^" + this.value + "$" : this.value

      let index = $(this).parent().index()
      if (visibleColumnsIndexes !== null) {
        index = visibleColumnsIndexes[index]
      }

      table
        .column(index)
        .search(value, strict)
        .draw()
  });
table.on('column-visibility.dt', function(e, settings, column, state) {
      visibleColumnsIndexes = []
      table.columns(":visible").every(function(colIdx) {
          visibleColumnsIndexes.push(colIdx);
      });
  })
})

</script>
@endsection