<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLotteriesTable extends Migration
{
    public function up()
    {
        Schema::create('lotteries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->longText('info');
            $table->integer('price');
            $table->longText('conditions');
            $table->datetime('deadline');
            $table->string('on_off');
            $table->string('name_en')->nullable();
            $table->longText('info_en')->nullable();
            $table->longText('conditions_en')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
