@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.lottery.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.lotteries.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.lottery.fields.id') }}
                        </th>
                        <td>
                            {{ $lottery->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.lottery.fields.on_off') }}
                        </th>
                        <td>
                            {{ App\Models\Lottery::ON_OFF_SELECT[$lottery->on_off] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.lottery.fields.category') }}
                        </th>
                        <td>
                            {{ $lottery->category->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.lottery.fields.name') }}
                        </th>
                        <td>
                            {{ $lottery->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.lottery.fields.name_en') }}
                        </th>
                        <td>
                            {{ $lottery->name_en }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.lottery.fields.info') }}
                        </th>
                        <td>
                            {{ $lottery->info }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.lottery.fields.info_en') }}
                        </th>
                        <td>
                            {{ $lottery->info_en }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.lottery.fields.price') }}
                        </th>
                        <td>
                            {{ $lottery->price }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.lottery.fields.conditions') }}
                        </th>
                        <td>
                            {!! $lottery->conditions !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.lottery.fields.conditions_en') }}
                        </th>
                        <td>
                            {!! $lottery->conditions_en !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.lottery.fields.deadline') }}
                        </th>
                        <td>
                            {{ $lottery->deadline }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.lottery.fields.photo') }}
                        </th>
                        <td>
                            @if($lottery->photo)
                                <a href="{{ $lottery->photo->getUrl() }}" target="_blank" style="display: inline-block">
                                    <img src="{{ $lottery->photo->getUrl('thumb') }}">
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.lottery.fields.winner') }}
                        </th>
                        <td>
                            {{ $lottery->winner->email ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.lotteries.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection