<?php

namespace App\Http\Requests;

use App\Models\Lottery;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreLotteryRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('lottery_create');
    }

    public function rules()
    {
        return [
            'on_off'      => [
                'required',
            ],
            'category_id' => [
                'required',
                'integer',
            ],
            'name'        => [
                'string',
                'required',
            ],
            'name_en'     => [
                'string',
                'nullable',
            ],
            'info'        => [
                'required',
            ],
            'price'       => [
                'required',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'conditions'  => [
                'required',
            ],
            'deadline'    => [
                'required',
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
            ],
            'photo'       => [
                'required',
            ],
        ];
    }
}
