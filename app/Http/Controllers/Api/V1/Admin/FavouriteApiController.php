<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFavouriteRequest;
use App\Http\Requests\UpdateFavouriteRequest;
use App\Http\Resources\Admin\FavouriteResource;
use App\Models\Favourite;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FavouriteApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('favourite_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new FavouriteResource(Favourite::with(['lottery', 'user'])->get());
    }

    public function store(StoreFavouriteRequest $request)
    {
        $favourite = Favourite::create($request->all());

        return (new FavouriteResource($favourite))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Favourite $favourite)
    {
        abort_if(Gate::denies('favourite_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new FavouriteResource($favourite->load(['lottery', 'user']));
    }

    public function update(UpdateFavouriteRequest $request, Favourite $favourite)
    {
        $favourite->update($request->all());

        return (new FavouriteResource($favourite))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Favourite $favourite)
    {
        abort_if(Gate::denies('favourite_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $favourite->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
