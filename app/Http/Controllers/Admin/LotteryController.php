<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyLotteryRequest;
use App\Http\Requests\StoreLotteryRequest;
use App\Http\Requests\UpdateLotteryRequest;
use App\Models\Category;
use App\Models\Lottery;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class LotteryController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('lottery_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $lotteries = Lottery::with(['category', 'winner', 'media'])->get();

        $categories = Category::get();

        $users = User::get();

        return view('admin.lotteries.index', compact('lotteries', 'categories', 'users'));
    }

    public function create()
    {
        abort_if(Gate::denies('lottery_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = Category::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $winners = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.lotteries.create', compact('categories', 'winners'));
    }

    public function store(StoreLotteryRequest $request)
    {
        $lottery = Lottery::create($request->all());

        if ($request->input('photo', false)) {
            $lottery->addMedia(storage_path('tmp/uploads/' . basename($request->input('photo'))))->toMediaCollection('photo');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $lottery->id]);
        }

        return redirect()->route('admin.lotteries.index');
    }

    public function edit(Lottery $lottery)
    {
        abort_if(Gate::denies('lottery_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = Category::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $winners = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $lottery->load('category', 'winner');

        return view('admin.lotteries.edit', compact('categories', 'winners', 'lottery'));
    }

    public function update(UpdateLotteryRequest $request, Lottery $lottery)
    {
        $lottery->update($request->all());

        if ($request->input('photo', false)) {
            if (!$lottery->photo || $request->input('photo') !== $lottery->photo->file_name) {
                if ($lottery->photo) {
                    $lottery->photo->delete();
                }

                $lottery->addMedia(storage_path('tmp/uploads/' . basename($request->input('photo'))))->toMediaCollection('photo');
            }
        } elseif ($lottery->photo) {
            $lottery->photo->delete();
        }

        return redirect()->route('admin.lotteries.index');
    }

    public function show(Lottery $lottery)
    {
        abort_if(Gate::denies('lottery_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $lottery->load('category', 'winner');

        return view('admin.lotteries.show', compact('lottery'));
    }

    public function destroy(Lottery $lottery)
    {
        abort_if(Gate::denies('lottery_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $lottery->delete();

        return back();
    }

    public function massDestroy(MassDestroyLotteryRequest $request)
    {
        Lottery::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('lottery_create') && Gate::denies('lottery_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Lottery();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
