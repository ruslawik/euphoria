@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.lottery.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.lotteries.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required">{{ trans('cruds.lottery.fields.on_off') }}</label>
                <select class="form-control {{ $errors->has('on_off') ? 'is-invalid' : '' }}" name="on_off" id="on_off" required>
                    <option value disabled {{ old('on_off', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Models\Lottery::ON_OFF_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('on_off', 'on') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('on_off'))
                    <span class="text-danger">{{ $errors->first('on_off') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.lottery.fields.on_off_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="category_id">{{ trans('cruds.lottery.fields.category') }}</label>
                <select class="form-control select2 {{ $errors->has('category') ? 'is-invalid' : '' }}" name="category_id" id="category_id" required>
                    @foreach($categories as $id => $category)
                        <option value="{{ $id }}" {{ old('category_id') == $id ? 'selected' : '' }}>{{ $category }}</option>
                    @endforeach
                </select>
                @if($errors->has('category'))
                    <span class="text-danger">{{ $errors->first('category') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.lottery.fields.category_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.lottery.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.lottery.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name_en">{{ trans('cruds.lottery.fields.name_en') }}</label>
                <input class="form-control {{ $errors->has('name_en') ? 'is-invalid' : '' }}" type="text" name="name_en" id="name_en" value="{{ old('name_en', '') }}">
                @if($errors->has('name_en'))
                    <span class="text-danger">{{ $errors->first('name_en') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.lottery.fields.name_en_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="info">{{ trans('cruds.lottery.fields.info') }}</label>
                <textarea class="form-control {{ $errors->has('info') ? 'is-invalid' : '' }}" name="info" id="info" required>{{ old('info') }}</textarea>
                @if($errors->has('info'))
                    <span class="text-danger">{{ $errors->first('info') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.lottery.fields.info_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="info_en">{{ trans('cruds.lottery.fields.info_en') }}</label>
                <textarea class="form-control {{ $errors->has('info_en') ? 'is-invalid' : '' }}" name="info_en" id="info_en">{{ old('info_en') }}</textarea>
                @if($errors->has('info_en'))
                    <span class="text-danger">{{ $errors->first('info_en') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.lottery.fields.info_en_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="price">{{ trans('cruds.lottery.fields.price') }}</label>
                <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="number" name="price" id="price" value="{{ old('price', '') }}" step="1" required>
                @if($errors->has('price'))
                    <span class="text-danger">{{ $errors->first('price') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.lottery.fields.price_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="conditions">{{ trans('cruds.lottery.fields.conditions') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('conditions') ? 'is-invalid' : '' }}" name="conditions" id="conditions">{!! old('conditions') !!}</textarea>
                @if($errors->has('conditions'))
                    <span class="text-danger">{{ $errors->first('conditions') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.lottery.fields.conditions_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="conditions_en">{{ trans('cruds.lottery.fields.conditions_en') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('conditions_en') ? 'is-invalid' : '' }}" name="conditions_en" id="conditions_en">{!! old('conditions_en') !!}</textarea>
                @if($errors->has('conditions_en'))
                    <span class="text-danger">{{ $errors->first('conditions_en') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.lottery.fields.conditions_en_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="deadline">{{ trans('cruds.lottery.fields.deadline') }}</label>
                <input class="form-control datetime {{ $errors->has('deadline') ? 'is-invalid' : '' }}" type="text" name="deadline" id="deadline" value="{{ old('deadline') }}" required>
                @if($errors->has('deadline'))
                    <span class="text-danger">{{ $errors->first('deadline') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.lottery.fields.deadline_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="photo">{{ trans('cruds.lottery.fields.photo') }}</label>
                <div class="needsclick dropzone {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="photo-dropzone">
                </div>
                @if($errors->has('photo'))
                    <span class="text-danger">{{ $errors->first('photo') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.lottery.fields.photo_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="winner_id">{{ trans('cruds.lottery.fields.winner') }}</label>
                <select class="form-control select2 {{ $errors->has('winner') ? 'is-invalid' : '' }}" name="winner_id" id="winner_id">
                    @foreach($winners as $id => $winner)
                        <option value="{{ $id }}" {{ old('winner_id') == $id ? 'selected' : '' }}>{{ $winner }}</option>
                    @endforeach
                </select>
                @if($errors->has('winner'))
                    <span class="text-danger">{{ $errors->first('winner') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.lottery.fields.winner_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/lotteries/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', '{{ $lottery->id ?? 0 }}');
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

<script>
    Dropzone.options.photoDropzone = {
    url: '{{ route('admin.lotteries.storeMedia') }}',
    maxFilesize: 10, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 10,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="photo"]').remove()
      $('form').append('<input type="hidden" name="photo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="photo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($lottery) && $lottery->photo)
      var file = {!! json_encode($lottery->photo) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, file.preview)
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="photo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
@endsection