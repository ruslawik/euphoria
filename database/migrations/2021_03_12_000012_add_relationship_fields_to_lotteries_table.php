<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToLotteriesTable extends Migration
{
    public function up()
    {
        Schema::table('lotteries', function (Blueprint $table) {
            $table->unsignedBigInteger('winner_id')->nullable();
            $table->foreign('winner_id', 'winner_fk_3347714')->references('id')->on('users');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id', 'category_fk_3408133')->references('id')->on('categories');
        });
    }
}
