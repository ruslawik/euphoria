<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToTicketsTable extends Migration
{
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id', 'user_fk_3347597')->references('id')->on('users');
            $table->unsignedBigInteger('lottery_id');
            $table->foreign('lottery_id', 'lottery_fk_3347598')->references('id')->on('lotteries');
        });
    }
}
