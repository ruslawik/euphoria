<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'lottery_create',
            ],
            [
                'id'    => 18,
                'title' => 'lottery_edit',
            ],
            [
                'id'    => 19,
                'title' => 'lottery_show',
            ],
            [
                'id'    => 20,
                'title' => 'lottery_delete',
            ],
            [
                'id'    => 21,
                'title' => 'lottery_access',
            ],
            [
                'id'    => 22,
                'title' => 'currency_create',
            ],
            [
                'id'    => 23,
                'title' => 'currency_edit',
            ],
            [
                'id'    => 24,
                'title' => 'currency_show',
            ],
            [
                'id'    => 25,
                'title' => 'currency_delete',
            ],
            [
                'id'    => 26,
                'title' => 'currency_access',
            ],
            [
                'id'    => 27,
                'title' => 'favourite_create',
            ],
            [
                'id'    => 28,
                'title' => 'favourite_edit',
            ],
            [
                'id'    => 29,
                'title' => 'favourite_show',
            ],
            [
                'id'    => 30,
                'title' => 'favourite_delete',
            ],
            [
                'id'    => 31,
                'title' => 'favourite_access',
            ],
            [
                'id'    => 32,
                'title' => 'ticket_create',
            ],
            [
                'id'    => 33,
                'title' => 'ticket_edit',
            ],
            [
                'id'    => 34,
                'title' => 'ticket_show',
            ],
            [
                'id'    => 35,
                'title' => 'ticket_delete',
            ],
            [
                'id'    => 36,
                'title' => 'ticket_access',
            ],
            [
                'id'    => 37,
                'title' => 'category_create',
            ],
            [
                'id'    => 38,
                'title' => 'category_edit',
            ],
            [
                'id'    => 39,
                'title' => 'category_show',
            ],
            [
                'id'    => 40,
                'title' => 'category_delete',
            ],
            [
                'id'    => 41,
                'title' => 'category_access',
            ],
            [
                'id'    => 42,
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
