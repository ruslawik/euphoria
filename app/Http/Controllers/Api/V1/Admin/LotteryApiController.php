<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreLotteryRequest;
use App\Http\Requests\UpdateLotteryRequest;
use App\Http\Resources\Admin\LotteryResource;
use App\Models\Lottery;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LotteryApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('lottery_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new LotteryResource(Lottery::with(['category', 'winner'])->get());
    }

    public function store(StoreLotteryRequest $request)
    {
        $lottery = Lottery::create($request->all());

        if ($request->input('photo', false)) {
            $lottery->addMedia(storage_path('tmp/uploads/' . basename($request->input('photo'))))->toMediaCollection('photo');
        }

        return (new LotteryResource($lottery))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Lottery $lottery)
    {
        abort_if(Gate::denies('lottery_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new LotteryResource($lottery->load(['category', 'winner']));
    }

    public function update(UpdateLotteryRequest $request, Lottery $lottery)
    {
        $lottery->update($request->all());

        if ($request->input('photo', false)) {
            if (!$lottery->photo || $request->input('photo') !== $lottery->photo->file_name) {
                if ($lottery->photo) {
                    $lottery->photo->delete();
                }

                $lottery->addMedia(storage_path('tmp/uploads/' . basename($request->input('photo'))))->toMediaCollection('photo');
            }
        } elseif ($lottery->photo) {
            $lottery->photo->delete();
        }

        return (new LotteryResource($lottery))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Lottery $lottery)
    {
        abort_if(Gate::denies('lottery_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $lottery->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
