<?php

namespace App\Http\Requests;

use App\Models\Lottery;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyLotteryRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('lottery_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:lotteries,id',
        ];
    }
}
